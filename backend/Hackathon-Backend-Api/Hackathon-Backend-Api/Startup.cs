using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hackathon_Backend_Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Tokens;
using System.Reflection;
using Newtonsoft;
using Microsoft.AspNetCore.Identity;
using System.Text;
using Hackathon_Backend_Api.Service;
using Hackathon_Backend_Api.Interfaces;

namespace Hackathon_Backend_Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            /*services.AddIdentity<IdentityUser, IdentityRole>(
                cfg => cfg.User.RequireUniqueEmail = true
            );*/

            services.AddHttpContextAccessor();
            services.AddControllers();

            services.AddScoped<IUser, UserRepository>();
            //   services.AddScoped<IDependencyOne, DependencyOne>();    < --I was missing this line!
            //services.AddScoped<IDependencyTwoThatIsDependentOnDependencyOne, DependencyTwoThatIsDependentOnDependencyOne>();

            services.AddScoped<IListingDetails, ListingDetailsRespository>();

            services.AddScoped<IListingDetails, ListingDetailsRespository>();

            services.AddDbContext<DatabaseContext>(
                option => option.UseSqlServer(Configuration.GetConnectionString("UserConnectionString"))
                );
<<<<<<< HEAD
            /* services.AddAuthentication().AddCookie().AddJwtBearer(
                 cfg =>
                 {
                     cfg.TokenValidationParameters = new TokenValidationParameters()
                     {
                         ValidateIssuer = true,
                         ValidateAudience = false,
                         IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("testKeyToken"))
                     };
                 }
                 );*/
=======
           /* services.AddAuthentication().AddCookie().AddJwtBearer(
                cfg =>
                {
                    cfg.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("testKeyToken"))
                    };
                }
                );*/
>>>>>>> 96408a1be593dc1d01506efa2e83e86453bd82ef

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}