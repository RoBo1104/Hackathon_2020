﻿using Hackathon_Backend_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api.Interfaces
{
    public interface IListingDetails
    {
        public List<ListingDetails> allListingDetails();
        public List<ListingDetails> getListingDetailsByUser(User user);

        public List<ListingDetails> getListingDetailsByCategory(Category category);
        public List<ListingDetails> getListingDetailsByUsername(string username);
        public List<ListingDetails> getListingDetailsByEmail(string email);
    }
}
