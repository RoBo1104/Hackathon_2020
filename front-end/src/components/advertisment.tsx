import { Fragment } from "react";
import { IJob } from "../interfaces/IJob";

interface Advertisment{
    ads: IJob[]
    isOwner: boolean
}

export const Advertisment : React.FC<Advertisment>  = (jobs, isOwner : boolean = false) => {
    var toDisplay = (
        <Fragment>
            <div style={{paddingLeft: "30px", paddingRight: "30px", paddingTop: "10px"}}>
                <div className="ui three doubling stackable cards">
                    {                                                   
                        jobs.ads.map((value,index) => {
                            console.log(isOwner);
                            var buttons = isOwner === true ? 
                                (  
                                    <Fragment>
                                        <div className="extra content">
                                            <button className="ui button purple">View</button>
                                            <button className="ui button">Delete</button>
                                        </div>
                                    </Fragment>
                                ) : 
                                (
                                    <Fragment>
                                        <div className="extra content">
                                            <button className="ui button purple">View</button>
                                        </div>
                                    </Fragment>
                                )
                            
                            
                            var append = 
                                 <div className="ui card" key={index}>
                                    <div className="image">
                                        <div className="ui placeholder">
                                        <div className="square image"></div>
                                    </div>
                                    </div>
                                    <div className="content">
                                        <div className="ui placeholder">
                                            <div className="header">
                                            <div className="very short line"></div>
                                            <div className="medium line"></div>
                                        </div>
                                        <div className="paragraph">
                                            <div className="short line"></div>
                                        </div>
                                        </div>
                                    </div>
                                    {buttons}
                                </div>
                            return append;
                        })
                    }
                </div>
            </div>
        </Fragment>
    )

    return(
        <Fragment>
            {toDisplay}
        </Fragment>   
    );
}