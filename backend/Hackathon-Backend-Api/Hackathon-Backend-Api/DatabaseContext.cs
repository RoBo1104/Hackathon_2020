﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hackathon_Backend_Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hackathon_Backend_Api
{
    public class DatabaseContext : DbContext
    {
        private IConfiguration _config;

        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
    : base(options)
        { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<User> User { get; set; }

        public DbSet<ListingDetails> ListingDetails { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            base.OnConfiguring(optionBuilder);
            optionBuilder.UseSqlServer(_config["ConnectionStrings:UserConnectionString"]);
        }
    }
}
