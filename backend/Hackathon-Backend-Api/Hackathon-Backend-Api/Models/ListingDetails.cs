﻿using System;
using System.Collections.Generic;
<<<<<<< HEAD
using System.ComponentModel.DataAnnotations;
=======
using System.ComponentModel.DataAnnotations.Schema;
>>>>>>> 96408a1be593dc1d01506efa2e83e86453bd82ef
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api.Models
{
    [Table("ListingDetails", Schema = "dbo")]
    public class ListingDetails
    {
        [Key]
        public int id { get; set; }
        public User employer { get; set; }
        public string Title { get; set; }
        public Category category { get; set; }
        public string SalaryRange { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public DateTime PostedAt { get; set; }
        public DateTime DueAt { get; set; }
    }
}
