import axios, {AxiosResponse} from 'axios'
import { ICompany } from '../interfaces/ICompany';
import { IJob } from '../interfaces/IJob';
import { IUser } from '../interfaces/IUsers';

const sleep = (delay: number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, delay)
    })
};

axios.defaults.baseURL = 'http://localhost:8882/api/';

axios.interceptors.response.use( async response => {
    try{
        await sleep(1500);
        return response;
    } catch (error){
        console.log(error);
        return await Promise.reject(error);
    }
});

const responseBody = <T> (response : AxiosResponse<T>) => response.data;

const request = {
    get: (url: string) => axios.get(url).then(responseBody),
    getByUser: (url: string, body: {}) => axios.post(url,body).then(responseBody),
    post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
    put: (url: string, body: {}) => axios.post(url, body).then(responseBody),
    delete: (url: string) => axios.delete(url).then(responseBody)
};

const User = {
    getUsers: (): Promise<IUser[]> => request.get(''),  
    getUser: (user: IUser): Promise<IUser> => request.post('', user),
    insertUser: (user: IUser): Promise<IUser> => request.post('user/register', user),
    updateUser: (user: IUser): Promise<IUser> => request.put('', user),
    deleteUser: (id: number): Promise<void> => request.delete(`/${id}`)  
};

const Job = {
    getJobs: (): Promise<IJob[]> => request.get(''),  
    getJob: (): Promise<IJob[]> => request.get(''),
    insertJob: (job: IJob): Promise<IJob> => request.post('/', job),
    updateJob: (job: IJob): Promise<IJob> => request.put('', job),
    deleteJob: (id: number): Promise<void> => request.delete(`/${id}`)  
}

const Company = {
    getCompanies: (): Promise<ICompany[]> => request.get(''),  
    getCompany: (): Promise<ICompany[]> => request.get(''),
    insertCompany: (company: ICompany): Promise<ICompany> => request.post('/', company),
    updateCompany: (company: ICompany): Promise<ICompany> => request.put('', company),
    deleteCompany: (id: number): Promise<void> => request.delete(`/${id}`)  
}

const agent = {
    User,
    Job,
    Company
}

export default agent;