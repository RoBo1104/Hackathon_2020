import { observer } from 'mobx-react-lite';
import { Fragment } from 'react';
import React, { useEffect } from 'react'
import { Header, Icon, Image } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { useStore } from '../stores/store';

export const Register = observer(() =>{
    
    const {userStore} = useStore();
    
    let confirmPassword: string = '';
    
    function validateEmail(email: string) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
    
    let validatePassword = () => {
        if(userStore.user.Password != userStore.user.ConfirmPassword){
            alert("Invalid passwords!")
            return false;
        }
        return true;
    }

    let validateUser = () => {
        if(!validateEmail(userStore.user.Email)){
            alert("Invalid email!")
            return false;
        }
        return true;
    }

    let registerButton = () => {
        if(validatePassword() && validateUser())
            userStore.insertOrUpdate(userStore.user);        
    }
    
    return(
        <Fragment>
            <div className="page-login" style={{paddingTop: '25vh'}}>
                <Header as='h2' icon textAlign='center'>
                    <Icon name='users' circular />
                    <Header.Content>Register in</Header.Content>
                </Header>
                <div className="ui centered grid container">
                    <div className="nine wide column">
                        <div className="ui fluid card">
                            <div className="context">
                                <div className="ui form" style={{padding: '30px'}}>
                                    <div className="field">
                                        <input type="text" name="Username" 
                                            value={userStore.user.Username} 
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => userStore.handlerChanges('Username',event)} 
                                            placeholder="User"/>
                                    </div>
                                    <div className="field">
                                        <input type="text" name="Email" 
                                            value={userStore.user.Email} 
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => userStore.handlerChanges('Email',event)} 
                                            placeholder="Email"/>
                                    </div>
                                    <div className="field">
                                        <input type="password" name="Password" 
                                                value={userStore.user.Password} 
                                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => userStore.handlerChanges('Password',event)}
                                                placeholder="Password"/>
                                    </div>
                                    <div className="field">
                                        <input type="password" name="ConfirmPassword" 
                                                value={userStore.user.ConfirmPassword} 
                                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => userStore.handlerChanges('ConfirmPassword',event)}
                                                placeholder="Confirm password"/>
                                    </div>
                                    <button className="ui primary labeled icon button" onClick={registerButton}>
                                        <i className="unlock alternate icon"></i>
                                        Register
                                    </button>
                                    <Link className="ui purple labeled icon button" to="/login">
                                        <i className="unlock alternate alternate icon"></i>
                                        Login
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
})