﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Threading.Tasks;

namespace webApi.Mappings
{
    public class ClassConfig : EntityTypeConfiguration<Clasa>
    {
        public ClassConfig()
        {
            ToTable('nume tabel');
            HasKey(x => x.keyeprimara);
            Property(x => x.keye).IsRequired(); // fara null
            Property(x => x.keye).IsOptional(); // cu null
        }
    }
}
