﻿using Hackathon_Backend_Api.Models;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Hackathon_Backend_Api
{
    public class UserRepository : IUser
    {
        private DatabaseContext context;

        public UserRepository(DatabaseContext _context)
        {
            _context = context;
        }

        User IUser.Add(User _user)
        {
            var userEntity = context.User.AddAsync(_user);
            context.SaveChanges();
            return _user;
        }

        Boolean IUser.Delete(int id)
        {
            try
            {
                User userToBeDeleted = (User)context.User.Where(x => x.id == id).ToList().Select(x => new User()
                {
                    id = x.id,
                    Username = x.Username,
                    isDeleted = x.isDeleted
                });
                context.Remove(userToBeDeleted.id);
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw;
            }
            return true;
        }


        List<User> IUser.GetAll()
        {
            return context.User.Where(x => x.isDeleted == false).ToList();
        }

        User IUser.Update(User _user)
        {
            try
            {
                context.User.Where(x => x.Username == _user.Username);
                context.User.Update(_user);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
            return _user;
        }

        User IUser.GetById(int id)
        {
            return (User)context.User.Where(x => x.isDeleted == false && x.id == id).FirstOrDefault();
        }

        User IUser.GetByUsername(string username)
        {
            return (User)context.User.Where(x => x.isDeleted == false && x.Username == username).FirstOrDefault();
        }  
    }
}