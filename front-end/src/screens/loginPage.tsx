import { observer } from 'mobx-react-lite';
import React, { Fragment } from 'react';
import { Header, Icon } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { useStore } from '../stores/store';

export const Login = observer(() =>{
   
    const {userStore} = useStore();

    var buttonHandle = () => {
        userStore.user = userStore.getUser(userStore.user);
    }

    return(
        <Fragment>
            <div className="page-login" style={{paddingTop: '25vh'}}>
                <Header as='h2' icon textAlign='center'>
                    <Icon name='users' circular />
                    <Header.Content>Log in</Header.Content>
                </Header>
                <div className="ui centered grid container">
                    <div className="nine wide column">
                        <div className="ui fluid card">
                            <div className="context">
                                <div className="ui form" style={{padding: '30px'}}>
                                    <div className="field">
                                        <input type="text" name="Username" id="Username" value={userStore.user.Username} 
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => userStore.handlerChanges('Username',event)} 
                                            placeholder="User"/>
                                    </div>
                                    <div className="field">
                                        <input type="password" id="Password" name="password" value={userStore.user.Password} 
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => userStore.handlerChanges('Password',event)} 
                                            placeholder="Password"/>
                                    </div>
                                    <button className="ui primary labeled icon button" onClick={buttonHandle}>
                                        <i className="unlock alternate icon"></i>
                                        Login
                                    </button>
                                    <Link className="ui purple labeled icon button" to="/register">
                                        <i className="unlock alternate alternate icon"></i>
                                        Register
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
})