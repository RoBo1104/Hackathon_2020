﻿using Hackathon_Backend_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api
{
    interface IUser
    {
        public User Add(User _object);
        public User Update(User _object);
        public List<User> GetAll();
        public User GetById(int id);
        public Boolean Delete(int id);
        public User GetByUsername(string username);

    }
}
