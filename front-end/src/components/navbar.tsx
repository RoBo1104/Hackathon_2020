import { Link } from "react-router-dom"
import { IUser } from "../interfaces/IUsers"

export const Navbar = (props: {user?: IUser} ) => {

    var isLogged;

    if(props.user != undefined && props.user.Id == 0 && props.user.Username == '')
        isLogged = (
            <div className="">
                <Link className="" to='/login'>Log In</Link>
            </div>
        );

    
    return(
            <div className="ui large menu">
                <span>
                    <Link className="active item" to="homepage">
                        Home Page
                    </Link>
                </span>
                <span>
                    <Link className="item" to="myPosts">
                        My posts
                    </Link>
                </span>
                <span>
                    <Link className="item" to="newPost">
                        New Post
                    </Link>
                </span>
                <div className="right menu">
                    <div className="ui dropdown item">
                        {isLogged}
                    </div>
                </div>
            </div>
    )
}