﻿using Hackathon_Backend_Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        UserService _userService;
        public SignInManager<User> _signInManager;

        public UserController(UserService userService, SignInManager<User> signInManager)
        {
            _userService = userService;
            _signInManager = signInManager;
        }

        [HttpPost]
        public IActionResult Register(LoginRequest loginRequest)
        {
            User user = new User();
            user.Username = loginRequest.Username;
            user.Password = loginRequest.Password;
            user.Email = loginRequest.Email;
            if(_userService.AddUser(user) != null)
            {
                return Ok(loginRequest);
            }else
            {
                return BadRequest(new { message = "Something went wrong" });
            }

        }

        [HttpPost]
        public IActionResult Login(LoginRequest loginRequest)
        {
            User user = _userService.GetUserByUsername(loginRequest.Username);
            if (user != null && user.Password.Equals(loginRequest.Password))
            {
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("Username", user.Username)
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Secret Jwt")), SecurityAlgorithms.HmacSha256Signature)
                };
                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(tokenDescriptor);
                var token = handler.WriteToken(securityToken);
                LoginResponse loginResponse = new LoginResponse();
                loginResponse.Username = loginRequest.Username;
                loginResponse.jwtToken = token;
                return Ok(loginResponse);
            } else
            {
                return BadRequest(new { message = "Username or password incorrect" });
            }

        }
    }
}
