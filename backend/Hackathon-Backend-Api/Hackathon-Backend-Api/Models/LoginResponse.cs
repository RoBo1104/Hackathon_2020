﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api
{
    public class LoginResponse
    {
        public string Username { get; set; }
        public string jwtToken { get; set; }
    }
}
