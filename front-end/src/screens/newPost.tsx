import { observer } from "mobx-react-lite"
import { Fragment } from "react"
import { Form, Input, Select, TextArea, Checkbox, Button } from "semantic-ui-react"
import { IJob } from "../interfaces/IJob"
import { useStore } from "../stores/store"

export const newPost = observer(() =>{
    
    const {jobStore} = useStore();

    const options = [
        { key: 'm', text: 'Male', value: 'male' },
        { key: 'f', text: 'Female', value: 'female' },
        { key: 'o', text: 'Other', value: 'other' },
      ]

    const skillSets = [
        { key: '1', text: 'C#', value: 'c#' },
        { key: '2', text: 'C++', value: 'c++' },
        { key: '3', text: 'Good Communication', value: 'goodc' },
        { key: '4', text: 'Good Listener', value: 'gool' },
        { key: '5', text: 'Advanced English Speaker', value: 'aes' },
        { key: '6', text: 'Advanced French Speaker', value: 'afs' },
        { key: '7', text: 'Advanced German Speaker', value: 'ags' },
    ]
    
    return (
        <div style={{width: "70%", padding: "10px;", paddingTop: "20hv", marginLeft: "30vh", marginTop:"10vh"}}>
            <Fragment>
                <Form>
                    <Form.Group widths='equal'>
                    <Form.Field
                        control={Input}
                        label='Job name'
                        placeholder='Job Name'
                    />
                    <Form.Field
                        control={Select}
                        label='Looking for'
                        options={options}
                        placeholder='Gender'
                    />
                    </Form.Group>
                    
                    <Form.Field
                    control={TextArea}
                    label='About'
                    placeholder='Tell us more about the job description...'
                    />
                    <Form.Field
                        control={Select}
                        label='Skillsets'
                        options={skillSets}
                        placeholder='Skills'
                    />
                    <Form.Field
                    control={Checkbox}
                    label='I agree to the Terms and Conditions'
                    />
                    <Form.Field control={Button}>Submit</Form.Field>
                </Form>
            </Fragment>
        </div>
    )
})