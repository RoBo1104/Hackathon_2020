﻿using Hackathon_Backend_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api
{
    public class UserService
    {
        private IUser _user;

        UserService(IUser user)
        {
            _user = user;
        }

        public User GetUserByUserId(int userId)
        {
            return _user.GetById(userId);
        }

        public IEnumerable<User> GetAllUsers()
        {
            try
            {
                return _user.GetAll().ToList();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public User GetUserByUsername(string username)
        {
            return _user.GetByUsername(username);
        }
        public bool DeleteUser(int userId)
        {
            try
            {
                return _user.Delete(userId);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public User UpdateUser(User user)
        {
            try
            {
                return _user.Update(user);
            }
            catch (Exception ex)
            {
                throw ;
            }
        }

        public User AddUser(User user)
        {
            try
            {
                return _user.Add(user);
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}
