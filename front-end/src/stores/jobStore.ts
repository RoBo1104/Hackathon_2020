import { makeAutoObservable } from "mobx"
import React from "react";
import agent from "../api/agent";
import { IJob } from "../interfaces/IJob";

export default class JobStore{

    job : IJob = {
        Id: 0,
        Username: "",
        Email: "",
        Category: "",
        SalaryRange: "",
        Description: "",
        Location: "",
        PostedAt: "",
        DueAt: ""
    }

    constructor(){
        makeAutoObservable(this);
    }

    jobs: IJob[] = [];

    handlerChanges = (changeValue : string, event: React.ChangeEvent<HTMLInputElement>) => {
        switch(changeValue){
            case 'Username' : {
                this.job.Username = (event.target.value);
                break;
            }
            case 'Email' : {
                this.job.Email = (event.target.value);
                break;
            }
            case 'Category' : {
                this.job.Category = (event.target.value);
                break;
            }
            case 'SalaryRange' : {
                this.job.SalaryRange = (event.target.value);
                break;
            }
            case 'Description' : {
                this.job.Description = (event.target.value);
                break;
            }
            case 'Location' : {
                this.job.Location = (event.target.value);
                break;
            }
            case 'DueAt' : {
                this.job.PostedAt = (event.target.value);
                break;
            }
        }
    }

    insertOrUpdate = (job: IJob) => {
        if(this.job.Id == 0)
            try {
                const newUser = agent.Job.insertJob(job);
            }
            catch(error) {
                console.log(error);
            }
    }

    getJobs = () => {
        agent.Job.getJobs().then((promise: IJob[]) => {
            this.jobs = promise;
        })
    }

    deleteByID = (id: number) => {
        agent.User.deleteUser(id);
    }
}