import React, { Fragment } from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";

import {Login} from './screens/loginPage'
import { Register } from './screens/registerPage';
import { Navbar } from './components/navbar';
import { IUser } from './interfaces/IUsers';
import { Homepage } from './screens/homepage';
import { newPost } from './screens/newPost';

export default function App() {
  
  var data : IUser = {
    Id: 0,
    Username: '',
    Email: '',
    Password: '',
    IsDelted: false,
    ConfirmPassword: ''
  }
  
  return (
    <Fragment>
      <Router>
            <Navbar user={data}/>
            <Route exact path="/homepage" component={Homepage} />
            <Route exact path="/newPost" component={newPost} />
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register} />
      </Router>
    </Fragment>
  );
}