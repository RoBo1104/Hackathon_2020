﻿using Hackathon_Backend_Api.Interfaces;
using Hackathon_Backend_Api.Models;
using Hackathon_Backend_Api.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api
{
    [Route("api/[controller]")]
    [ApiController]
<<<<<<< HEAD
    public class ListingDetailsController : Controller
    {
        IListingDetails _listingDetailsService;
        public ListingDetailsController(IListingDetails listingDetailsService)
        {
            _listingDetailsService = listingDetailsService;
        }
        [HttpGet("allDetails")]
=======
    public class ListingDetailsController : ControllerBase
    {
        ListingDetailsService _listingDetailsService;
      public ListingDetailsController(ListingDetailsService listingDetailsService)
        {
            _listingDetailsService = listingDetailsService;
        }
        [HttpGet("getAllJobs")]
>>>>>>> 96408a1be593dc1d01506efa2e83e86453bd82ef
        public IActionResult getAllJobListings()
        {
            List<ListingDetails> allListingDetailsObjects = _listingDetailsService.allListingDetails();
            if(allListingDetailsObjects != null && allListingDetailsObjects.Count != 0)
            {
                return Ok(allListingDetailsObjects);
            } else
            {
                return BadRequest(new { message = "Something went wrong" });
            }
        }


        [HttpGet("something")]
        public IActionResult getJobListingByUser(RequestJobListings requestJobListings)
        {
            List<ListingDetails> allListingDetailsObjectsByUsername = _listingDetailsService.getListingDetailsByUsername(requestJobListings.username);
            if (allListingDetailsObjectsByUsername != null && allListingDetailsObjectsByUsername.Count != 0)
            {
                return Ok(allListingDetailsObjectsByUsername);
            }
            List<ListingDetails> allListingDetailsObjectsByEmail = _listingDetailsService.getListingDetailsByEmail(requestJobListings.email);
            if (allListingDetailsObjectsByEmail != null && allListingDetailsObjectsByEmail.Count != 0)
            {
                return Ok(allListingDetailsObjectsByEmail);
            } else
            {
                return BadRequest(new { message = "Something went wrong" });
            }
        }

    }
}
