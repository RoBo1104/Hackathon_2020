import { observer } from "mobx-react-lite"
import { Fragment } from "react"
import { Advertisment } from "../components/advertisment";
import { IJob } from "../interfaces/IJob";

export const Homepage = observer(() =>{
    
    var IJob : IJob = {
        Username: "",
        Email: "",
        Category: "",
        SalaryRange: "",
        Description: "",
        Location: "",
        PostedAt: "",
        DueAt: "",
        Id: 0
    }

    var IJobs : IJob[] = [
        IJob,
        IJob,
        IJob,
        IJob,
        IJob,
        IJob
    ]
    
    return (
        <Fragment>
            <Advertisment ads={IJobs} isOwner={true} />
        </Fragment>
    );
})