﻿using Hackathon_Backend_Api.Interfaces;
using Hackathon_Backend_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api.Service
{
    public class ListingDetailsRespository : IListingDetails
    {

        public ListingDetailsRespository()
        {
        }
        List<ListingDetails> IListingDetails.allListingDetails()
        {
            using (var context = new DatabaseContext())
            {
                return context.ListingDetails.ToList();
            }
        }

        List<ListingDetails> IListingDetails.getListingDetailsByCategory(Category category)
        {
            using (var context = new DatabaseContext())
            {
                return context.ListingDetails.Where(x => x.category.Equals(category)).ToList();
            }
        }

        List<ListingDetails> IListingDetails.getListingDetailsByUser(User user)
        {
            using (var context = new DatabaseContext())
            {
                return context.ListingDetails.Where(x => x.employer.Equals(user)).ToList();
            }
        }

        List<ListingDetails> IListingDetails.getListingDetailsByUsername(string username)
        {
            using (var context = new DatabaseContext())
            {
                return context.ListingDetails.Where(x => x.employer.Username.Equals(username)).ToList();
            }
        }

        List<ListingDetails> IListingDetails.getListingDetailsByEmail(string email)
        {
            using (var context = new DatabaseContext())
            {
                return context.ListingDetails.Where(x => x.employer.Email.Equals(email)).ToList();
            }
        }

    }
}
