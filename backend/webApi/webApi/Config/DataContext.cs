﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace webApi.Config
{
    public class DataContext : DbContext
    {
        public DbSet<ClasaVirtualaDinDomain> Clasa { get; set; }
        public DataContext() : base("string")
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            Database.SetInitializer<DataContext>(new Initializer<DataContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Configuration());              
        }
    }
}
