﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace webApi.Config
{
    public class Initializer<T> : IDatabaseInitializer<T> where T: DbContext
    {
        public void InitializeDatabase(T context) {
            if (!context.Database.Exists())
            {

            }
        }
    }
}
