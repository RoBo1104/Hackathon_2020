﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api
{
    public class LoginRequest
    {
        [Required]
        [MinLength(5)]
        [MaxLength(250)]
        public string Username { get; set; }
        public string Email { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(250,ErrorMessage="Mesaj prea lung")]
        public string Password { get; set; }
    }
}
