import { makeAutoObservable } from "mobx"
import React from "react";
import agent from "../api/agent";
import {IUser} from "../interfaces/IUsers"

export default class UserStore{
    user : IUser = {
        Id: 0,
        Username: "",
        Email: "",
        Password: "",
        ConfirmPassword: "",
        IsDelted: false
    };

    constructor(){
        makeAutoObservable(this);
    }

    handlerChanges = (changeValue : string, event: React.ChangeEvent<HTMLInputElement>) => {
        switch(changeValue){
            case 'Username' : {
                this.user.Username = (event.target.value);
                break;
            }
            case 'Password' : {
                this.user.Password = (event.target.value);
                break;
            }
            case 'ConfirmPassword' : {
                this.user.ConfirmPassword = (event.target.value);
                break;
            }
            case 'Email' : {
                this.user.Email = (event.target.value);
                break;
            }
        }
    }

    insertOrUpdate = (user: IUser) => {
        if(this.user.Id == 0)
            try {
                const newUser = agent.User.insertUser(user).then( (promise: any) => console.log(promise));
            }
            catch(error) {
                console.log(error);
            }
    }

    deleteByID = (id: number) => {
        agent.User.deleteUser(id);
    }

    getUser = (user: IUser) => {
        try {
            agent.User.getUser(user).then((promise: IUser) => {
                this.user = promise;
            })
        } catch(error) {
            console.log(error)
        } finally {
            return this.user;
        }
    }
}