export interface IUser{
    Id: number,
    Username : string,
    Email: string,
    Password : string,
    ConfirmPassword: string,
    IsDelted: boolean
}