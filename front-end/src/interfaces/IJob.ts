export interface IJob{
    Id: 0,
    Username: string,
    Email: string,
    Category: string,
    SalaryRange: string,
    Description: string,
    Location: string,
    PostedAt: string,
    DueAt: string
}