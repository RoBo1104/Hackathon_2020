import { createContext, useContext } from "react";
import JobStore from "./jobStore";
import UserStore from "./userStore";

interface Store{
    jobStore: JobStore
    userStore: UserStore
    //companyStore: CompanyStore
}

export const store: Store =  {
    jobStore: new JobStore(),
    userStore: new UserStore()
    //companyStore: new CompanyStore()
}

export const StoreContext = createContext(store);

export function useStore(){
    return useContext(StoreContext);
}