﻿using Hackathon_Backend_Api.Interfaces;
using Hackathon_Backend_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon_Backend_Api.Service
{
    public class ListingDetailsService
    {
        private IListingDetails _listingDetails;

        ListingDetailsService(IListingDetails listingDetails)
        {
            _listingDetails = listingDetails;
        }

        public List<ListingDetails> allListingDetails()
        {
            return _listingDetails.allListingDetails();
        }

        public List<ListingDetails> getListingDetailsByCategory(Category category)
        {
            return _listingDetails.getListingDetailsByCategory(category);
        }

        public List<ListingDetails> getListingDetailsByUser(User user)
        {
            return _listingDetails.getListingDetailsByUser(user);
        }

        public List<ListingDetails> getListingDetailsByUsername(string username)
        {
            return _listingDetails.getListingDetailsByUsername(username);
        }

        public List<ListingDetails> getListingDetailsByEmail(string email)
        {
            return _listingDetails.getListingDetailsByEmail(email);
        }
    }
}
